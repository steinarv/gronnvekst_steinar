<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'gronnvekst_no' );

/** MySQL database username */
define( 'DB_USER', 'gronnvekst_no' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'BInvHikhkx268HzlUeW/dx0BthUR94qfvvy4a8XfiIrpmijivX4X1MkOn1Ksiz7Q7s2HRYlYmB95MX8jz4KjAA==');
define('SECURE_AUTH_KEY',  'OLkmzqc5mlXPCOlPcMx0wdsZFifbPpfDmpE4UTlvBeYVTKIrDSotpTmn7ewe0l8g7/IOh+cEnnRoJ/OHvXTBaQ==');
define('LOGGED_IN_KEY',    'j6YhAX4JlonsI8rc9Cqsv5pu8y/OFf6aynmABqzyqmvrjTX1aGzgNgmYhNspOLs9PH6AekssYwawXJMbNUjU4Q==');
define('NONCE_KEY',        'kQEH4T9IHTLZRpC/c0Ql1rmiiIUCPQQ+6J8qmRyVjaYEDcT80pz6HppiB4TVJTtT+XMnjtLd8WGDoCRPIWGnAw==');
define('AUTH_SALT',        'Ams+ra9+2JdVbQZ3LZxv3RA499sG5GfQQZK1RHHEeHiXHXAIve/N9OVWyL0koXmJRHwamYtMS8dsTGMIHTLKlw==');
define('SECURE_AUTH_SALT', 'u4lHyBMbCS1D0SMWvUE4vPoOKlPzDxz/12THKd2Wh6bvYZT+YP1VCFVEsmCMWGWOjC4so4P8AfxQ8yVTmwGVTA==');
define('LOGGED_IN_SALT',   'vlmiolh1C1D4LI9u7EEcwzfaiY3rtdQxEuWUnWjh/OAWq1i/sq5q3baoyeMF1IFPByoSWSuxXCs4ymIwrRbi0g==');
define('NONCE_SALT',       'v0RhIHQv9tfii88PknJMXb1VUccHPxtSVRsOqHMEgOHVSkT9YvLb97r7/Gkp3kSWQqIZcaxf5uXurt1N4zspsg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';





/* Inserted by Local by Flywheel. See: http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy */
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
	$_SERVER['HTTPS'] = 'on';
}

/* Inserted by Local by Flywheel. Fixes $is_nginx global for rewrites. */
if (strpos($_SERVER['SERVER_SOFTWARE'], 'Flywheel/') !== false) {
	$_SERVER['SERVER_SOFTWARE'] = 'nginx/1.10.1';
}
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
