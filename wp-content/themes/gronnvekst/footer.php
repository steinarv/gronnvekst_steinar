<?php
/**
 * The template for displaying the footer. 
 *
 * Comtains closing divs for header.php.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */			
 ?>
					
				<footer class="footer" role="contentinfo">
					
				</footer> <!-- end .footer -->
			

		</div> <!-- end #body-wrap -->
		
		<?php wp_footer(); ?>
		
	</body>
	
</html> <!-- end page -->