<?php
/*
Template Name: Jordkalkulator
*/

get_header(); ?>
			
	<div class="content">
      <div class="calculator-wrapper">
        <form id="calc-form">
          <section class="bg-white calc-item calc-zip">
            <div class="section-wrap">
              <h2>Hvor ønsker du ordren levert?</h2>
              <h2 class="calc-zip-ok hidden">Du har valgt levering i 3050 Mjøndalen</h2>
              <h2 class="clac-zip-error hidden">Beklager, vi har ingen utsalgssteder i dette området</h2>
              <div class="calc-info-wrapper">
                <p class="calc-info">Skriv inn postnummer slik at vi finner ditt nærmeste utsalgssted.</p>
                <p class="calc-info-ok hidden">Ditt nærmeste utsalgssted er Lyngås i Lier. </p>
                <p class="clac-info-error hidden">Kontakt oss gjerne, så skal vi se om vi kan hjelpe deg videre</p>
              </div>
              <div class="input-wrapper">
                <div class="button-inline">
                  <input type="text" class="input-zip" /><input type="button" value="Ok" />
                </div>
              </div>
            </div>
          </section>
          <section class="bg-dark calc-item calc-products">
            <div class="section-wrap">
              <h2>Hvor mye jord trenger du?</h2>
              <div class="calc-info-wrapper">
                <p>Her kan du beregne mengden du trenger. Felt for høyde gjelder komprimert jord. Volum vises i ukomprimert jord.</p>
              </div>
              <div class="calc-product">
                <header>
                  <span class="remove icon-cross"></span>
                  <h3><a data-toggle="extra-info">Beregn mengde for <span class="calc-product-name">Hage-Mix &reg;</span></a> </h3>
                  <div id="extra-info" data-toggler=".expanded">
                    <p>Hage-Mix ® er en stabil, næringsrik og lett moldjord til nyanlegg og utskifting av jord.</p>
                    <p><a href="#">Mer info</a></p>
                  </div>
                </header>
                <div class="volume-inputs">
                  <div class="input-wrapper label-top input-areal">
                    <label for="areal">Areal</label>
                    <input id="areal" type="text" />
                    <span class="def">m<sup>2</sup></span>
                  </div>
                  <div class="input-wrapper label-top input-height">
                    <label for="height">Høyde</label>
                    <input id="height" type="text" />
                    <span class="def">cm</span>
                    <span class="help">(komprimert)</span>
                  </div>
                  <div class="input-wrapper label-top input-volume">
                    <label for="volume">Volum</label>
                    <input id="volume" type="text" />
                    <span class="def">m<sup>3</sup></span>
                    <span class="help">(ukomprimert)</span>
                  </div>
                </div>
                <div class="totals">
                  <span class="amount">
                    4 400,-
                  </span>
                  <span class="reset"><span class="icon-clean"></span>Nullstill</span>
                </div>
              </div>
              <p class="calc-total">
                Totalt <span class="amount">8 800,-</span>
              </p>
              <p class="button-wrap button-radius button-add-products">
                <a data-toggle="panel-add-products body-wrap"><span class="icon-plus"></span> Legg til produkt</a>
              </p>
              <div class="panel" id="panel-add-products" data-toggler=".expanded">
                <header>
                  <span class="back-button" data-toggle="panel-add-products body-wrap"><span class="icon-arrow-right"></span> </span>
                  <h3>Legg til produkt</h3>
                  <div class="hidden-flex"></div>
                </header>
                <div class="panel-content">
                  <p style="color: red;">Her hentes dynamisk woocommerce-produkter</p>
                  <h4>Jord</h4>
                  <ul>
                    <li>
                      <a href="#">
                        Hage-Jord
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        Park-Mix&reg;
                      </a>
                    </li>
                  </ul>
                  <p class="button-wrap button-close">
                    <span class="back-button" data-toggle="panel-add-products body-wrap">Lukk <span class="icon-cross"></span></span>
                  </p>
                </div>
              </div>
            </div>
          </section>
          <section class="bg-medium calc-item calc-delivery">
            <div class="section-wrap">
              <h2>Hvordan ønsker du jorden levert?</h2>
              <div class="calc-info-wrapper">
                <p>Vi kan levere både med tipp, grabb og i storsekk. Klikk på ønsket  leveringsmetode.</p>
                <p class="button-wrap button-radius button-info">
                  <a data-toggle="panel-info-delivery body-wrap">Les: Viktig info om levering!</a>
                </p>
              </div>
              <ul class="delivery-options">
                <li class="tipp">
                  <span><label for="tipp"><input type="radio" name="delivery" value="tipp" id="tipp"> Tipp</label></span>
                </li>
                <li class="grabb">
                  <span><label for="grabb"><input type="radio" name="delivery" value="grabb" id="grabb"> Grabb</label></span>
                </li>
                <li class="storsekk">
                  <span><label for="storsekk"><input type="radio" name="delivery" value="storsekk" id="storsekk"> Storsekk</label></span>
                </li>
              </ul>
              <p class="delivery-total">
                <span class="text">Pris frakt</span>
                <span class="amount">4 400,-</span>
              </p>

              <div class="panel" id="panel-info-delivery" data-toggler=".expanded">
                <header>
                  <span class="back-button" data-toggle="panel-info-delivery body-wrap"><span class="icon-arrow-right"></span> </span>
                  <h3>Viktig info om levering</h3>
                  <div class="hidden-flex"></div>
                </header>
                <div class="panel-content">
                  <p style="color: red;">Her hentes dynamisk innholdet fra en page</p>
                  <p class="button-wrap button-close">
                    <span class="back-button" data-toggle="panel-info-delivery body-wrap">Lukk <span class="icon-cross"></span></span>
                  </p>
                </div>
              </div>
            </div>
          </section>
          <section class="bg-white calc-item calc-schedule">
            <div class="section-wrap">
              <h2>Når ønsker du jorden levert?</h2>
              <div class="calc-info-wrapper">
                <p>Standard levering er innen 2-3 virkedager, men du kan også bestille levering på lørdager eller neste dag for et pristillegg.</p>
              </div>
              <ul class="schedule-options">
                <li class="three-days">
                  <span><label for="three-days"><input type="radio" name="schedule" value="three-days" id="three-days" checked> 2-3 virkedager</label></span>
                </li>
                <li class="saturday">
                  <span><label for="saturday"><input type="radio" name="schedule" value="saturday" id="saturday"> Lørdag (+ 1 500,-)</label></span>
                </li>
                <li class="next-day">
                  <span><label for="next-day"><input type="radio" name="schedule" value="next-day" id="next-day"> Neste dag (+ 1 200,-)</label></span>
                </li>
              </ul>
            </div>
          </section>
          <section class="bg-dark calc-item calc-summary">
            <div class="section-wrap">
              <h2>Vi har beregnet frakt av <span class="summary-volume">96</span> m³ jord fra <span class="summary-store">Lier</span> til <span class="summary-zip">3050 Mjøndalen</span>.
                Dette tilsvarer <span class="summary-transport">11 lastebiler.</span></h2>
              <div class="calc-info-wrapper">
                <p>Du har bestilt levering med <span class="summary-delivery">grab</span>. <span class="summary-volume1">12 m3 løsmasse</span> og <span class="summary-volume2">1 big-bag.</span></p>
              </div>
              <table class="summary">
                <tr class="table-product">
                  <td class="table-product-name">
                    Hage-mix
                  </td>
                  <td class="table-volume">
                    14m3
                  </td>
                  <td class="table-sum">
                    6 700,-
                  </td>
                </tr>
                <tr class="hr">
                  <td colspan="3"><hr></td>
                </tr>
                <tr>
                  <td colspan="2" class="table-delivery">
                    Frakt
                  </td>
                  <td class="table-sum">
                    5 600,-
                  </td>
                </tr>
                <tr>
                  <td colspan="2" class="table-delivery">
                    Tillegg lørdagslevering
                  </td>
                  <td class="table-sum">
                    1 200,-
                  </td>
                </tr>
                <tr class="hr">
                  <td colspan="3"><hr></td>
                </tr>
                <tr class="table-total">
                  <td colspan="2">
                    Totalt
                  </td>
                  <td>
                    31 000,-
                  </td>
                </tr>
              </table>
              <p>Alle priser inkludert mva.</p>
              <p class="button-wrap button-buy">
                <input type="submit" class="buy" value="Gå til bestilling" />
              </p>
            </div>
          </section>
        </form>
      </div>
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<?php endwhile; endif; ?>							


	
	</div> <!-- end #content -->

<?php get_footer(); ?>
