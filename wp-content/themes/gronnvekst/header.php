<?php
/**
 * The template for displaying the header
 *
 * This is the template that displays all of the <head> section
 *
 */
?>

<!doctype html>

  <html class="no-js"  <?php language_attributes(); ?>>

	<head>
		<meta charset="utf-8">
		
		<!-- Force IE to use the latest rendering engine available -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta class="foundation-mq">
		
		<!-- If Site Icon isn't set in customizer -->
		<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
			<!-- Icons & Favicons -->
			<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
			<link href="<?php echo get_template_directory_uri(); ?>/assets/images/apple-icon-touch.png" rel="apple-touch-icon" />	
	    <?php } ?>

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
      <link rel="stylesheet" href="https://use.typekit.net/gan6dfh.css">
		<?php wp_head(); ?>

	</head>
			
	<body <?php body_class(); ?>>
    <div id="body-wrap" data-toggler=".overlay">
    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" style="display:none">
      <symbol id="email-icon" viewBox="0 0 64 64"><path d="M17,22v20h30V22H17z M41.1,25L32,32.1L22.9,25H41.1z M20,39V26.6l12,9.3l12-9.3V39H20z"/></symbol>
      <symbol id="email-mask" viewBox="0 0 64 64"><path d="M41.1,25H22.9l9.1,7.1L41.1,25z M44,26.6l-12,9.3l-12-9.3V39h24V26.6z M0,0v64h64V0H0z M47,42H17V22h30V42z"/></symbol>
      <symbol id="facebook-icon" viewBox="0 0 64 64"><path d="M34.1,47V33.3h4.6l0.7-5.3h-5.3v-3.4c0-1.5,0.4-2.6,2.6-2.6l2.8,0v-4.8c-0.5-0.1-2.2-0.2-4.1-0.2 c-4.1,0-6.9,2.5-6.9,7V28H24v5.3h4.6V47H34.1z"/></symbol>
      <symbol id="facebook-mask" viewBox="0 0 64 64"><path d="M0,0v64h64V0H0z M39.6,22l-2.8,0c-2.2,0-2.6,1.1-2.6,2.6V28h5.3l-0.7,5.3h-4.6V47h-5.5V33.3H24V28h4.6V24 c0-4.6,2.8-7,6.9-7c2,0,3.6,0.1,4.1,0.2V22z"/></symbol>
    </svg>

      <div class="row">
        <div class="medium-12 columns">
          <header class="masthead" role="banner">

            <!-- This navs will be applied to the topbar, above all content
				 To see additional nav styles, visit the /parts directory -->
            <div class="logo">
              <a href="<?php echo home_url(); ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/gronn-vekst-logo.png" />
              </a>
            </div>
            <div class="menu-container">
              <a href="#menu" class="menu-link">Meny</a>
              <nav id="menu" role="navigation" class="mainmenu">
                  <?php joints_top_nav(); ?>
              </nav>
                <div id="sqs-social" class="social-links sqs-svg-icon--list">
                  <a href="mailto:post@gronnvekst.no" target="_blank" class="sqs-svg-icon--wrapper email">
                    <div>
                      <svg class="sqs-svg-icon--social" viewBox="0 0 64 64">
                        <use class="sqs-use--icon" xlink:href="#email-icon"></use>
                        <use class="sqs-use--mask" xlink:href="#email-mask"></use>
                      </svg>
                    </div>
                  </a>
                  <a href="https://www.facebook.com/Grønn-Vekst-366321523474241/timeline/" target="_blank" class="sqs-svg-icon--wrapper facebook">
                    <div>
                      <svg class="sqs-svg-icon--social" viewBox="0 0 64 64">
                        <use class="sqs-use--icon" xlink:href="#facebook-icon"></use>
                        <use class="sqs-use--mask" xlink:href="#facebook-mask"></use>
                      </svg>
                    </div>
                  </a>
                </div>
            </div>
          </header> <!-- end .header -->
        </div>
      </div>
