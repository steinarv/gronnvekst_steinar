<?php
/**
 * Plugin Name: GodtSagt Jordkalkulator
 * Plugin URI: https://www.godtsagt.no/
 * Description: Kalkulator for e-handelsløsning for bestilling av jord.
 */


defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

register_activation_hook(__FILE__,'create_upgrade_database');
add_action( 'add_meta_boxes', 'legg_inn_pris_postnummer_meta' );
add_action( 'admin_enqueue_scripts', 'legge_inn_adminscript' );
add_action('save_post','lagre_postnummer_priser');
add_action('init','start_session',1);


$GLOBALS['postnummer_vars'] = array(array('db' => 'salgssted', type => 1, 'html' => '','data' => 0),
	array('db' => 'postnummer', type => 0, 'html' => 'gj_postnummer','data' => 0),
	array('db' => 'poststed', type => 0, 'html' => 'gj_poststed','data' => 0),
	array('db' => 'kommunenummer', type => 0, 'html' => 'gj_kommunenummer','data' => 0),
	array('db' => 'kommunenavn', type => 0, 'html' => 'gj_kommunenavn','data' => 0),
	array('db' => 'standard_pris', type => 0, 'html' => 'gj_standard','data' => 1),
	array('db' => 'grabb_pris', type => 0, 'html' => 'gj_grabb','data' => 1),
	array('db' => 'kran_pris', type => 0, 'html' => 'gj_kran','data' => 1),
	array('db' => 'lordag_pris', type => 0, 'html' => 'gj_lordag','data' => 1),
	array('db' => 'dagsleveranse_pris', type => 0, 'html' => 'gj_dagsleveranse','data' => 1));

$GLOBALS['session_objekt'] = null;

function start_session(){
	if(!session_id()) {
		session_start();
	}
	echo session_id();
}

function lagre_postnummer_priser(){
	global $wpdb;
	global $post;
	$storrelse = count($_POST['gj_id']);
	for($a = 0;$a < $storrelse; $a++){
		if($_POST['gj_id'][$a] == 0 && $_POST['gj_slettmeg'][$a] == 0){
			$insert_sporring = postnummer_insert_sporring($a,$post->ID,$wpdb);
			$wpdb->query($insert_sporring);
		}else if($_POST['gj_slettmeg'][$a] == 1){
			$wpdb->query("DELETE FROM {$wpdb->prefix}postnummer_pris WHERE id = ".intval($_POST['gj_id'][$a]).";");
		}else{
			$update_sporring = postnummer_update_sporring($a,$post->ID,$wpdb);
			echo $update_sporring.'<br />';
			$wpdb->query($update_sporring);
		}
	}
}

function postnummer_update_sporring($a,$id,$wpdb){
	$data = '';
	foreach($GLOBALS['postnummer_vars'] as $variab){
		if(strlen($variab['html']) > 0) {
			if ( $data != '' ) {
				$data .= ',';
			}
			if ( $variab['data'] == 0 ) {
				$data .= $variab['db']." = '".$_POST[$variab['html']][$a]."'";
			} else {
				$data .= $variab['db']." = ".$_POST[$variab['html']][$a];
			}
		}
	}
	return "UPDATE {$wpdb->prefix}postnummer_pris SET ".$data." WHERE id = ".$_POST['gj_id'][$a].";";

}

function postnummer_insert_sporring($a,$id,$wpdb){
	$start = "salgssted";
	$slutt = $id;
	foreach($GLOBALS['postnummer_vars'] as $variab){
		if(strlen($variab['html']) > 0) {
			$start .= "," . $variab['db'];
			if ( $variab['data'] == 0 ) {
				$slutt .= ",'".$_POST[$variab['html']][$a]."'";
			} else {
				$slutt .= "," . $_POST[$variab['html']][$a];
			}
		}
	}
	return "INSERT INTO {$wpdb->prefix}postnummer_pris(".$start.") VALUES(".$slutt.");";
}


function write_form_line($linje){
	?><tr>
	<?php

	foreach($GLOBALS['postnummer_vars'] as $variab){
		if(strlen($variab['html']) > 0) {
			?>
			<td>
			<input type="text" name="<?php echo $variab['html']; ?>[]" value="<?php echo $linje->$variab['db']; ?>"/>
			</td><?php
		}
	}

	?>
	<td>
		<input type="hidden" name="gj_id[]" value="<?php echo $linje->id; ?>" />
		<input type="hidden" name="gj_slettmeg[]" value="0" class="gj_slettfelt" />
		<button class="gj_slett_postnummer_knapp">Slett</button>
	</td>
	</tr><?php
}


function legge_inn_adminscript()
{

	wp_register_script( 'adminjs', plugins_url( '/js/gj_admin.js', __FILE__ ), array('jquery'));
	wp_enqueue_script( 'adminjs' );
	wp_localize_script( 'adminjs', 'postnummer_admin', array('ajax_url' => admin_url('admin-ajax.php')));
}

function legg_inn_pris_postnummer_meta(){
	add_meta_box('postnummer_pris_box', 'Legg inn og endre priser for enkelte postnummer', 'legg_inn_pris_postnummer_box', 'utsalgssted');
}

function legg_inn_pris_postnummer_box(){
	global $wpdb;
	global $post;
	$postID = $post->ID;
	$results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}postnummer_pris WHERE salgssted = ".$postID, OBJECT );
	?><table id="gj_postnummer_tabell"><thead><tr><th>Postnummer</th><th>Poststed</th><th>Kommunenummer</th><th>Kommunenavn</th><th>Standard pris</th><th>Pris grabb</th><th>Pris kran</th><th>Pris dagsleveranse</th><th>Pris Lørdag</th></tr></thead>
	<tbody><?php
	foreach($results as $result){
		write_form_line($result);
	}
	?></tbody></table>
	<button id="gj_leggtil_postnummer_knapp">Legg til ny</button><?php
}



function create_upgrade_database(){
	global $wpdb;

	$karaktersett = $wpdb->get_charset_collate();
	$tabellnavn_postnummer = $wpdb->prefix.'postnummer_pris';
	$tabellnavn_kalksession = $wpdb->prefix.'calc_session';

	$sql_postnummer = "CREATE TABLE $tabellnavn_postnummer (
			id mediumint(9) NOT NULL AUTO_INCREMENT,
			postnummer mediumint(9),
			poststed varchar(90),
			kommunenummer mediumint(9),
			kommunenavn varchar(120),
			salgssted mediumint(9), 
			standard_pris float, 
			grabb_pris float,
			kran_pris float,
			dagsleveranse_pris float,
			lordag_pris float,
			PRIMARY KEY(id)
			)$karaktersett;";

	$sql_kalkulatorsession = "CREATE $tabellnavn_kalksession (
	    id mediumint(9) NOT NULL AUTO_INCREMENT,
	    session_id varchar(40),
	    session_objekt text,
	    opprettet datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
	    PRIMARY KEY(id)
	    )$karaktersett;";

	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	dbDelta($sql_kalkulatorsession);
	dbDelta($sql_postnummer);

}

