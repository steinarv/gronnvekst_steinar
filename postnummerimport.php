<?php

	include("wp-config.php");
	$conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME) or die("Kan ikke koble til database");
	$conn->set_charset("utf8");

	$import_rules = null;

	foreach(file('datagrunnlag.csv') as $line) {
		$bearbeid = explode(';',$line);
		if(!$import_rules){
			$import_rules = make_import_mapping($bearbeid, $conn);
		}else{
			$insert = 0;
			foreach($import_rules as $pos => $rule){
				if($rule['type'] == 1 && $bearbeid[$pos] == 'x'){
					$insert = $rule['exval'];
				}
			}

			if($insert > 0){
				mysqli_query($conn,"DELETE FROM wp_postnummer_pris WHERE salgssted = ".$insert." AND postnummer = ".$bearbeid[0].";");
				$start = "salgssted";
				$slutt = $insert;
				foreach($import_rules as $pos => $rule){
					if($rule['type'] == 0){
						$start .= ','.$rule['database'];
						$slutt .= ",'".utf8_encode(trim($bearbeid[$pos]))."'";
					}
				}
				$sql_sporring = 'INSERT INTO wp_postnummer_pris('.$start.') VALUES('.$slutt.');';
				mysqli_query($conn,$sql_sporring);
			}
		}
	}




	function make_import_mapping($linje, $conn){
		$modus = 0;
		$datasett = array();
		$pos = 0;
		foreach($linje as $ut){
			$data = trim(utf8_encode($ut));
			if($modus == 0){
				$datasett[$pos] = array('database' => 'postnummer', 'exval' => 0, 'type' => 0);
				$modus++;
			}else if($modus == 1){
				$datasett[$pos] = array('database' => 'poststed', 'exval' => 0, 'type' => 0);
				$modus++;
			}else if($modus == 2){
				$datasett[$pos] = array('database' => 'kommunenummer', 'exval' => 0, 'type' => 0);
				$modus++;
			}else if($modus == 3){
				$datasett[$pos] = array('database' => 'kommunenavn', 'exval' => 0, 'type' => 0);
				$modus++;
			}else if($modus == 4){
				if($data == 'XXX'){
					$datasett[$pos] = array('database' => '', 'exval' => 0, 'type' => -1);
					$modus++;
				}else{
					$finn_bedrift = mysqli_query($conn, "SELECT ID FROM wp_posts WHERE post_title = '".$data."' AND post_type = 'utsalgssted';");
					if($finn_bedrift->num_rows == 0){
						$datasett[$pos] = array('database' => 'salgssted', 'exval' => 0, 'type' => 1);
					}else{
						$resultat = mysqli_fetch_array($finn_bedrift);
						$datasett[$pos] = array('database' => 'salgssted', 'exval' => $resultat['ID'], 'type' => 1);
					}

				}
			}
			else if($modus == 5){
				$datasett[$pos] = array('database' => 'standard_pris', 'exval' => 0, 'type' => 0);
				$modus++;
			}else if($modus == 6){
				$datasett[$pos] = array('database' => 'grabb_pris', 'exval' => 0, 'type' => 0);
				$modus++;
			}else if($modus == 7){
				$datasett[$pos] = array('database' => 'kran_pris', 'exval' => 0, 'type' => 0);
				$modus++;
			}else if($modus == 8){
				$datasett[$pos] = array('database' => 'dagsleveranse_pris', 'exval' => 0, 'type' => 0);
				$modus++;
			}else if($modus == 9){
				$datasett[$pos] = array('database' => 'lordag_pris', 'exval' => 0, 'type' => 0);
				$modus++;
			}
			$pos++;
		}
		return $datasett;
	}


?>